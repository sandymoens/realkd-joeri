/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.propositions;

import static de.unibonn.realkd.data.constraints.Constraints.lowerQuantileBound;
import static de.unibonn.realkd.data.constraints.Constraints.lowerQuantileBoundNegation;
import static de.unibonn.realkd.data.constraints.Constraints.upperQuantileBound;
import static de.unibonn.realkd.data.constraints.Constraints.upperQuantileBoundNegation;

import java.util.List;

import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

/**
 * Factory methods for the creation of propositional logics.
 * 
 * @author Mario Boley
 * 
 * @since 0.5.1
 * 
 * @version 0.5.1
 *
 */
public class Propositions {

	private Propositions() {
		; // not to be instantiated
	}

	/**
	 * 
	 * @param table
	 *            the input table
	 * @return propositional logic for table based on default attribute to
	 *         proposition mapping scheme
	 */
	public static TableBasedPropositionalLogic propositionalLogic(DataTable table) {
		return new PropositionalLogicFromTableBuilder().build(table);
	}

	/**
	 * @param result
	 * @param ordinal
	 * @param c
	 */
	public static <T> void addLowerQuantileBoundBasedPropositions(List<AttributeBasedProposition<?>> result,
			OrdinalAttribute<T> ordinal, double c) {
		T threshold = ordinal.quantile(c);
		result.add(new DefaultAttributeBasedProposition<T>(ordinal, lowerQuantileBound(ordinal, threshold),
				result.size()));
		result.add(new DefaultAttributeBasedProposition<T>(ordinal, lowerQuantileBoundNegation(ordinal, threshold),
				result.size()));
	}

	/**
	 * @param result
	 * @param ordinal
	 * @param c
	 */
	public static <T> void addUpperQuantileBoundBasedPropositions(List<AttributeBasedProposition<?>> result,
			OrdinalAttribute<T> ordinal, double c) {
		T threshold = ordinal.quantile(c);
		result.add(new DefaultAttributeBasedProposition<T>(ordinal, upperQuantileBoundNegation(ordinal, threshold),
				result.size()));
		result.add(new DefaultAttributeBasedProposition<T>(ordinal, upperQuantileBound(ordinal, threshold),
				result.size()));
	}

}
