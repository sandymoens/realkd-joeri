/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common.measures;

import java.util.List;

/**
 * Represents the result of measuring some quantity.
 * 
 * @author Sandy Moens
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 * 
 * @see Measure
 *
 */
public interface Measurement {

	/**
	 * Identifies the quality measure for which value was taken.
	 * 
	 * @return id of measure
	 * 
	 */
	public Measure measure();

	/**
	 * Value for measure that was received in this measurement. Note that
	 * non-deterministic measurement procedures may produce different values on
	 * subsequent applications for the same pattern descriptor.
	 * 
	 * @return measure value
	 * 
	 */
	public double value();

	/**
	 * Provides list of other measurements that have been taken as auxiliary
	 * operation in order to compute this measurement.
	 * 
	 * @return list of measurements
	 * 
	 */
	public List<Measurement> auxiliaryMeasurements();

}
