/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.logical;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.common.IndexSets.difference;
import static de.unibonn.realkd.common.IndexSets.intersection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.2.0
 * 
 * @version 0.4.1
 *
 */
public class LogicalDescriptors {

	public static LogicalDescriptor create(PropositionalLogic propositionalLogic, Collection<Proposition> elements) {
		return LogicalDescriptorDefaultImplementation.create(propositionalLogic, elements);
	}

	/**
	 * <p>
	 * Computes a approximate shortest logical descriptor with identical
	 * extension.
	 * </p>
	 * <p>
	 * This implementation (version 0.4.1) uses the greedy set cover algorithm.
	 * It does not guarantee the solution to be minimal. Therefore it is
	 * followed up by one mapping to the lexicograpically last minimal
	 * generator.
	 * </p>
	 * 
	 * @version 0.4.1
	 * 
	 * @param x
	 *            some logical descriptor
	 * @return logical descriptor describing identical extension as x
	 * 
	 */
	public static LogicalDescriptor approximateShortestGenerator(LogicalDescriptor x) {
		IndexSet toCover = difference(x.population().objectIds(), x.supportSet());
		HashSet<Proposition> candidates = new HashSet<>(x.getElements());
		List<Proposition> solution = new ArrayList<>();
		while (!toCover.isEmpty()) {
			Proposition best = null;
			IndexSet smallestDifference = toCover;
			for (Proposition c : candidates) {
				IndexSet supportSet = x.getPropositionalLogic().supportSet(c.getId());
				IndexSet difference = IndexSets.intersection(toCover, supportSet);
				if (difference.size() < smallestDifference.size()) {
					smallestDifference = difference;
					best = c;
				}
			}
			solution.add(best);
			candidates.remove(best);
			toCover = smallestDifference;
		}
		LogicalDescriptorDefaultImplementation greedyApproximation = new LogicalDescriptorDefaultImplementation(
				x.getPropositionalLogic(), canonicalOrder(solution), x.supportSet());
		return greedyApproximation.lexicographicallyLastMinimalGenerator();
	}

	private static final Comparator<Proposition> PROPOSITION_ORDER = (p1, p2) -> Integer.compare(p1.getId(),
			p2.getId());// new CanonicalOrder();

	private static List<Proposition> canonicalOrder(Collection<Proposition> elements) {
		List<Proposition> orderedElements = new ArrayList<>(elements);
		Collections.sort(orderedElements, PROPOSITION_ORDER);
		return orderedElements;
	}

	/**
	 * <p>
	 * Implementation that buffers support set on creation; this is likely to be
	 * removed in future versions.
	 * </p>
	 */
	public static class LogicalDescriptorDefaultImplementation implements LogicalDescriptor {

		/**
		 * computes the support set of the pattern by forming the intersection
		 * of the support sets of all propositions. Note that in the special
		 * case of an empty description, the support set has to be the complete
		 * set of objects
		 */
		private static IndexSet calculateSupport(PropositionalLogic propositionalLogic, List<Proposition> elements) {
			if (elements.isEmpty()) {
				return propositionalLogic.population().objectIds();
			}

			IndexSet result = propositionalLogic.supportSet(elements.get(0).getId());
			for (int i = 1; i < elements.size(); i++) {
				result = intersection(result, propositionalLogic.supportSet(elements.get(i).getId()));
			}
			return result;
		}

		static LogicalDescriptor create(PropositionalLogic propositionalLogic, Collection<Proposition> elements) {
			List<Proposition> orderedElements = canonicalOrder(elements);
			IndexSet supportSet = calculateSupport(propositionalLogic, orderedElements);
			return new LogicalDescriptorDefaultImplementation(propositionalLogic, orderedElements, supportSet);
		}

		private final IndexSet supportSet;

		private final List<Proposition> elements;

		private final IndexSet elementIndexList;

		private final PropositionalLogic propositionalLogic;

		private LogicalDescriptorDefaultImplementation(PropositionalLogic propositionalLogic,
				List<Proposition> orderedElements, IndexSet supportSet) {
			this.propositionalLogic = propositionalLogic;
			this.elements = ImmutableList.copyOf(orderedElements);
			this.supportSet = supportSet;
			this.elementIndexList = IndexSets.of(getElements().stream().mapToInt(p -> p.getId()).toArray());
			// this.elementIndexList = getElements().stream().map(p ->
			// p.getId()).collect(toList());
		}

		@Override
		public PropositionalLogic getPropositionalLogic() {
			return propositionalLogic;
		}

		@Override
		public boolean minimal() {
			for (Proposition p : elements) {
				if (getGeneralization(p).supportSet().size() == supportSet().size()) {
					return false;
				}
			}
			return true;
		}

		@Override
		public LogicalDescriptor lexicographicallyLastMinimalGenerator() {
			LogicalDescriptor current = this;
			for (Proposition p : elements) {
				LogicalDescriptor generalization = current.getGeneralization(p);
				if (generalization.supportSet().size() == supportSet().size()) {
					current = generalization;
				}
			}
			return current;
		}

		/**
		 * 
		 * @return number of contained propositions
		 */
		@Override
		public int size() {
			return this.elements.size();
		}

		@Override
		public boolean isEmpty() {
			return this.elements.isEmpty();
		}

		@Override
		public Proposition getElement(int i) {
			return this.elements.get(i);
		}

		@Override
		public boolean empiricallyImplies(Proposition p) {
			// if (semanticallyImplies(p)) {
			// semImplCount++;
			// return true;
			// }
			return p.getSupportSet().containsAll(supportSet());
		}

		@Override
		public List<String> getElementsAsStringList() {
			List<String> descriptionList = new ArrayList<>();
			for (Proposition proposition : getElements()) {
				descriptionList.add(proposition.toString());
			}

			return descriptionList;
		}

		@Override
		public LogicalDescriptor getSpecialization(Proposition augmentation) {
			List<Proposition> newElements = new ArrayList<>(getElements());
			newElements.add(augmentation);
			Collections.sort(newElements, PROPOSITION_ORDER);

			IndexSet newSupportSet = intersection(supportSet, propositionalLogic.supportSet(augmentation.getId()));
			return new LogicalDescriptorDefaultImplementation(getPropositionalLogic(), newElements, newSupportSet);
		}

		@Override
		public LogicalDescriptor supportPreservingExtension(List<Proposition> augmentations) {
			List<Proposition> newElements = new ArrayList<>(getElements());
			boolean augmented = false;
			for (Proposition augmentation : augmentations) {
				if (getElements().contains(augmentation)) {
					continue;
				}
				if (propositionalLogic.holdsFor(augmentation.getId(), supportSet())) {
					newElements.add(augmentation);
					augmented = true;
				}
			}
			if (!augmented) {
				return this;
			} else {
				Collections.sort(newElements, PROPOSITION_ORDER);
				return new LogicalDescriptorDefaultImplementation(getPropositionalLogic(), newElements, supportSet);
			}
		}

		@Override
		public LogicalDescriptor getGeneralization(Proposition reductionElement) {
			if (!getElements().contains(reductionElement)) {
				throw new IllegalArgumentException("reduction element not part of description");
			}
			List<Proposition> newDescription = new ArrayList<>(getElements());
			newDescription.remove(reductionElement);
			return create(getPropositionalLogic(), newDescription);

		}

		@Override
		public boolean refersToAttribute(Attribute<?> attribute) {
			for (Proposition proposition : getElements()) {
				if (proposition instanceof AttributeBasedProposition
						&& ((AttributeBasedProposition<?>) proposition).getAttribute() == attribute) {
					return true;
				}
			}

			return false;
		}

		@Override
		public List<Attribute<?>> getReferencedAttributes() {
			List<Attribute<?>> result = new ArrayList<>();
			for (Proposition proposition : getElements()) {
				if (proposition instanceof AttributeBasedProposition<?>) {
					result.add(((AttributeBasedProposition<?>) proposition).getAttribute());
				}
			}
			return result;
		}

		@Override
		public List<Proposition> getElements() {
			return this.elements;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof LogicalDescriptor))
				return false;

			LogicalDescriptor other = (LogicalDescriptor) o;

			if (other.getElements().size() != this.getElements().size()) {
				return false;
			}

			for (int i = 0; i < this.getElements().size(); i++) {
				if (this.getElements().get(i) != other.getElements().get(i)) {
					return false;
				}
			}

			return true;
		}

		@Override
		public int hashCode() {
			return Objects.hash(elements);
		}

		@Override
		public String toString() {
			return elements.toString();
		}

		@Override
		public IndexSet supportSet() {
			return supportSet;
		}

		@Override
		public Population population() {
			return propositionalLogic.population();
		}

		@Override
		public SerialForm<LogicalDescriptor> serialForm() {
			return new LogicalDescriptorBuilderDefaultImplementation(getPropositionalLogic().identifier(),
					elements.stream().map(e -> e.getId()).collect(Collectors.toCollection(ArrayList::new)));
		}

		@Override
		public IndexSet elementIndexList() {
			return elementIndexList;
		}

	}

	public static SerialForm<LogicalDescriptor> logicalDescriptorBuilder(String propositionalLogicIdentifier) {
		return new LogicalDescriptorBuilderDefaultImplementation(propositionalLogicIdentifier, newArrayList());
	}

	public static class LogicalDescriptorBuilderDefaultImplementation implements SerialForm<LogicalDescriptor> {

		private final String propositionalLogicIdentifier;

		private final ArrayList<Integer> elementIndices;

		@JsonCreator
		public LogicalDescriptorBuilderDefaultImplementation(
				@JsonProperty("propositionalLogicIdentifier") String propositionalLogicIdentifier,
				@JsonProperty("elementIndexList") List<Integer> elementIndices) {
			this.propositionalLogicIdentifier = propositionalLogicIdentifier;
			this.elementIndices = Lists.newArrayList(elementIndices);
		}

		@Override
		public LogicalDescriptor build(Workspace context) {
			checkArgument(context.contains(propositionalLogicIdentifier, PropositionalLogic.class),
					"Workspace does not contain artifact '" + propositionalLogicIdentifier
							+ "' of type PropositionalLogic");
			PropositionalLogic propositionalLogic = (PropositionalLogic) context.get(propositionalLogicIdentifier);
			List<Proposition> propositions = elementIndices.stream().map(i -> propositionalLogic.propositions().get(i))
					.collect(Collectors.toList());
			return LogicalDescriptorDefaultImplementation.create(propositionalLogic, propositions);
		}

		public List<Integer> getElementIndexList() {
			return elementIndices;
		}

		public String getPropositionalLogicIdentifier() {
			return propositionalLogicIdentifier;
		}

		@Override
		public Collection<String> dependencyIds() {
			return ImmutableSet.of(getPropositionalLogicIdentifier());
		}

		// public void propositionalLogicIdentifier(String identifier) {
		// this.propositionalLogicIdentifier = identifier;
		// }

		@Override
		public boolean equals(Object other) {
			if (other == this) {
				return true;
			}
			if (!(other instanceof LogicalDescriptorBuilderDefaultImplementation)) {
				return false;
			}
			LogicalDescriptorBuilderDefaultImplementation otherDescriptor = (LogicalDescriptorBuilderDefaultImplementation) other;
			return (this.elementIndices.equals(otherDescriptor.elementIndices)
					&& this.propositionalLogicIdentifier.equals(otherDescriptor.propositionalLogicIdentifier));
		}

	}

}
