package de.unibonn.realkd.patterns;

import java.util.List;

import de.unibonn.realkd.data.table.attribute.Attribute;

/**
 * NOTE: This interface was originally created to allow to define that a pattern
 * descriptor refers to a subset of attributes from a table. However, there is
 * no way to attain a reference to the table in question.
 * 
 * @author Mario Boley
 *
 */
public interface TableSubspaceDescriptor extends PatternDescriptor {

	public List<Attribute<?>> getReferencedAttributes();

}
