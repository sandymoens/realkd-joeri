/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.util;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;

/**
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.0
 * 
 * @version 0.5.2
 *
 */
public class DefaultPropositionList implements PropositionList {
	
	public static PropositionList emptyPropositionList(PropositionalLogic propositionalLogic) {
		return new DefaultPropositionList(propositionalLogic, newArrayList());
	}
	
	public static PropositionList newPropositionList(PropositionalLogic propositionalLogic, List<Proposition> propositions) {
		return new DefaultPropositionList(propositionalLogic, propositions);
	}

	private PropositionalLogic propositionalLogic;
	private List<Proposition> propositions;

	private DefaultPropositionList(PropositionalLogic propositionalLogic, List<Proposition> propositions) {
		this.propositionalLogic = propositionalLogic;
		this.propositions = ImmutableList.copyOf(propositions);
	}
	
	@Override
	public List<Proposition> propositions() {
		return this.propositions;
	}

	@Override
	public PropositionListBuilder serialForm() {
		return new DefaultPropositionListBuilder(this.propositionalLogic.identifier(), 
				this.propositions.stream().map(p -> p.getId()).collect(Collectors.toList()));
	}
	
	@Override
	public boolean isEmpty() {
		return this.propositions().isEmpty();
	}

	@Override
	public PropositionList getSpecialization(Proposition augmentation) {
		List<Proposition> propositions = Lists.newArrayList(this.propositions());
		propositions.add(augmentation); 
		return newPropositionList(this.propositionalLogic, ImmutableList.copyOf(propositions));
	}

	@Override
	public PropositionList getGeneralization(Proposition reductionElement) {
		if (!this.propositions().contains(reductionElement)) {
			throw new IllegalArgumentException("reduction element not part of description");
		}
		List<Proposition> propositions = Lists.newArrayList(this.propositions());
		propositions.remove(reductionElement);
		return newPropositionList(this.propositionalLogic, ImmutableList.copyOf(propositions));
	}
	
	public String toString() {
		return "[" + String.join(", ", this.propositions.stream().map(p -> p.name()).collect(toList())) + "]"; 
	}
	
	public static PropositionListBuilder defaultPropositionListBuilder(String propositionalLogicIdentifier) {
		return new DefaultPropositionListBuilder(propositionalLogicIdentifier, newArrayList());
	}
	
	private static class DefaultPropositionListBuilder implements PropositionListBuilder {
		
		@JsonProperty("propositionalLogicIdentifier")
		private String propositionalLogicIdentifier;
		
		@JsonProperty("elementList") 
		private List<Integer> elementList;
		
		@JsonCreator
		public DefaultPropositionListBuilder(
				@JsonProperty("propositionalLogicIdentifier") String propositionalLogicIdentifier,
				@JsonProperty("elementList") List<Integer> elementList) {
			this.propositionalLogicIdentifier = propositionalLogicIdentifier;
			this.elementList = elementList;
		}
		
		@Override
		public PropositionList build(Workspace workspace) {
			PropositionalLogic propositionalLogic = (PropositionalLogic) workspace.get(this.propositionalLogicIdentifier);
			List<? extends Proposition> propositions = propositionalLogic.propositions();
			return new DefaultPropositionList(propositionalLogic,
					elementList.stream().map(i -> propositions.get(i)).collect(Collectors.toList()));
		}

		@Override
		public List<Integer> elementList() {
			return this.elementList;	
		}
		
	}

}
