/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.propositions;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.common.workspace.EntitySerialForm;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;

/**
 * @author Mario Boley
 * 
 * @since 0.2.0
 * 
 * @version 0.3.0
 *
 */
public class DefaultPropositionalLogic implements PropositionalLogic, HasSerialForm<PropositionalLogic> {
	
	private static class DefaultPropositionalLogicSerialForm implements EntitySerialForm<PropositionalLogic> {

		@JsonProperty("identifier")
		private final String identifier;

		@JsonProperty("name")
		private final String name;

		@JsonProperty("description")
		private final String description;

		@JsonProperty("propositions")
		private final List<PropositionSerialForm> propositionSerialForms;

		private final ImmutableList<String> dependencies;

		@JsonProperty("population")
		private Population population;

		@JsonCreator
		public DefaultPropositionalLogicSerialForm(@JsonProperty("identifier") String identifier,
				@JsonProperty("name") String name, @JsonProperty("description") String description,
				@JsonProperty("population") Population population,
				@JsonProperty("propositions") List<PropositionSerialForm> propositionSerialForms) {
			this.identifier = identifier;
			this.name = name;
			this.description = description;
			this.population = population;
			this.propositionSerialForms = propositionSerialForms;
			this.dependencies = ImmutableList.of();
		}

		@Override
		public String identifier() {
			return identifier;
		}

		public PropositionalLogic build(Workspace workspace) {
			List<Proposition> propositions = this.propositionSerialForms.stream().map(p -> new SetBackedProposition(p.id, p.name, IndexSets.copyOf(p.supportSet))).collect(Collectors.toList());
			return new DefaultPropositionalLogic(this.identifier, this.description, this.population, propositions);
		}

		public Collection<String> dependencyIds() {
			return dependencies;
		}

	}
	
	private static class PropositionSerialForm  {
		
		@JsonProperty("id")
		private Integer id;
		
		@JsonProperty("name")
		private String name;
		
		@JsonProperty("supportSet")
		private List<Integer> supportSet;
		
		public PropositionSerialForm(@JsonProperty("id") Integer id, @JsonProperty("name") String name, @JsonProperty("supportSet") List<Integer> supportSet) {
			this.id = id;
			this.name = name;
			this.supportSet = supportSet;
		}
		
	}

	// private static final Logger LOGGER = Logger
	// .getLogger(DefaultPropositionalLogic.class.getName());

	private final ImmutableList<Proposition> propositions;

	private final Set<Integer>[] truthSets;

	private final String name;
	
	private final String id;

	private final String description;

	private final Population population;

	@SuppressWarnings("unchecked")
	public DefaultPropositionalLogic(String id, String name, String description, Population population,
			List<Proposition> propositions) {
		this.id=id;
		this.name = name;
		this.description = description;
		this.population = population;
		this.propositions = ImmutableList.copyOf(propositions);
		this.truthSets = ((Set<Integer>[]) new Set[population.size()]);
	}

	public DefaultPropositionalLogic(String name, String description, Population population,
			List<Proposition> propositions) {
		this(name, name, description, population, propositions);
	}

	@Override
	public List<Proposition> propositions() {
		return propositions;
	}

	@Override
	public String toString() {
		return name();
	}

	@Override
	public String identifier() {
		return id;
	}
	
	@Override
	public String name() {
		return name;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public IndexSet supportSet(int basePropositionIndex) {
		return propositions.get(basePropositionIndex).getSupportSet();
	}

	@Override
	public Set<Integer> truthSet(int objectId) {
		if (truthSets[objectId] == null) {
			List<Integer> list = propositions.stream().filter(x -> x.holdsFor(objectId)).map(x -> x.getId())
					.collect(Collectors.toList());
			truthSets[objectId] = ImmutableSet.copyOf(list);
		}
		return truthSets[objectId];
	}

	@Override
	public Population population() {
		return population;
	}
	
	private static List<Integer> convertToList(IndexSet supportSet) {
		return StreamSupport.stream(supportSet.spliterator(), false).collect(Collectors.toList());
}

	@Override
	public SerialForm<? extends PropositionalLogic> serialForm() {
		List<PropositionSerialForm> propositionSerialForms = this.propositions.stream().map(p -> new PropositionSerialForm(p.getId(), p.name(), convertToList(p.getSupportSet()))).collect(Collectors.toList());
		
		return new DefaultPropositionalLogicSerialForm(this.id, this.name, this.description, this. population, propositionSerialForms);
	}


}
