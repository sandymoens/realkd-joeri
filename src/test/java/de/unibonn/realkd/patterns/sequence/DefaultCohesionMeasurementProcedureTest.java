/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.sequence;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static de.unibonn.realkd.patterns.sequence.DefaultCohesionMeasurementProcedure.INSTANCE;
import static de.unibonn.realkd.patterns.sequence.DefaultSequenceDescriptor.defaultSequenceDescriptorBuilder;
import static de.unibonn.realkd.patterns.util.DefaultPropositionList.defaultPropositionListBuilder;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTableBuilder;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalLogic;
import de.unibonn.realkd.data.sequences.SequenceDatabaseFromTableFactory;
import de.unibonn.realkd.data.sequences.SequentialPropositionalLogic;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTableFromCSVFileBuilder;
import de.unibonn.realkd.patterns.util.PropositionListBuilder;

/**
 * @author Sandy Moens
 *
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class DefaultCohesionMeasurementProcedureTest {
	
	private static final double PRECISION = 0.00000000001;
	
	private Workspace dataWorkspace;
	private TableBasedPropositionalLogic logic;
	private SequentialPropositionalLogic db;
	private Map<String, Integer> propositionMap;

	@Before
	public void setUp() {
		String dataFile = "src/test/resources/data/sequences/data.txt";
		String attributesFile = "src/test/resources/data/sequences/attributes.txt";
		DataTableFromCSVFileBuilder builder = new DataTableFromCSVFileBuilder().setDataCSVFilename(dataFile)
				.setAttributeMetadataCSVFilename(attributesFile);
		PropositionalLogicFromTableBuilder pBuilder = new PropositionalLogicFromTableBuilder();
		pBuilder.mappers(PropositionalLogicFromTableBuilder.DEFAULT_MAPPERS);
		try {
			logic = pBuilder.build(builder.build());
			
			propositionMap = newHashMap();
			logic.propositions().forEach(p -> propositionMap.put(p.toString(), p.getId()));
			
			SequenceDatabaseFromTableFactory f = new SequenceDatabaseFromTableFactory();
			f.groupingAttributeName("id");
			f.timeAttributeName("date");
			db = f.build(logic);
			dataWorkspace = Workspaces.workspace();
			dataWorkspace.add(logic);
			dataWorkspace.add(db);
		} catch (DataFormatException e) {
			e.printStackTrace();
		}
	}
	
	private PropositionListBuilder getList(String ...names) {
		PropositionListBuilder b = defaultPropositionListBuilder(db.identifier());
		for(String name: names) {
			b.elementList().add(propositionMap.get(name));
		}
		return b;
	}
	
	private void test(double expected, List<PropositionListBuilder> orderedSetBuilders) {
		SerialForm<SequenceDescriptor> builder = defaultSequenceDescriptorBuilder(db.identifier(), orderedSetBuilders);
		Assert.assertEquals(expected, INSTANCE.perform(builder.build(dataWorkspace)).value(), PRECISION);
	}
	
	@Test
	public void singleListSingleItemEqualsOne() {
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature2=value1"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature2=value2"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature2=value30"));
			test(1.0, orderedSetBuilders);
		}
	}
	
	@Test
	public void singleEventMultipleEvents() {
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1", "feature1=value2"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2", "feature1=value1"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1", "feature2=value1"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2", "feature2=value2"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2", "feature2=value1"));
			test(1.0/(4.0/3.0), orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2", "feature2=value3"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value20", "feature1=value3"));
			test(1, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value20", "feature2=value30"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value20", "feature1=value3", "feature2=value30"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1", "feature2=value10", "feature1=value2", "feature2=value1"));
			test(0.25, orderedSetBuilders);
		}
	}
	
	@Test
	public void multipleEventsMultipleEvents() {
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			orderedSetBuilders.add(getList("feature1=value2"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2"));
			orderedSetBuilders.add(getList("feature1=value1"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			orderedSetBuilders.add(getList("feature2=value1"));
			test(2.0/3.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2"));
			orderedSetBuilders.add(getList("feature2=value2"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2"));
			orderedSetBuilders.add(getList("feature2=value1"));
			test(2.0/(5.0/2.0), orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value2"));
			orderedSetBuilders.add(getList("feature2=value3"));
			test(Double.NaN, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3"));
			orderedSetBuilders.add(getList("feature2=value20", "feature1=value3"));
			test(Double.NaN, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value20"));
			orderedSetBuilders.add(getList("feature1=value3"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3"));
			orderedSetBuilders.add(getList("feature2=value20"));
			orderedSetBuilders.add(getList("feature1=value3"));
			test(Double.NaN, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3"));
			orderedSetBuilders.add(getList("feature2=value20", "feature2=value30"));
			test(Double.NaN, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value20"));
			orderedSetBuilders.add(getList("feature2=value30"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3"));
			orderedSetBuilders.add(getList("feature2=value20"));
			orderedSetBuilders.add(getList("feature2=value30"));
			test(Double.NaN, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3"));
			orderedSetBuilders.add(getList("feature2=value20", "feature1=value3", "feature2=value30"));
			test(Double.NaN, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value20"));
			orderedSetBuilders.add(getList("feature1=value3", "feature2=value30"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			orderedSetBuilders.add(getList("feature2=value10", "feature1=value2", "feature2=value1"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			orderedSetBuilders.add(getList("feature2=value10"));
			orderedSetBuilders.add(getList("feature1=value2", "feature2=value1"));
			test(0.75, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1", "feature2=value10"));
			orderedSetBuilders.add(getList("feature1=value2", "feature2=value1"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1", "feature2=value10", "feature1=value2"));
			orderedSetBuilders.add(getList("feature2=value1"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1", "feature1=value2", "feature2=value10"));
			orderedSetBuilders.add(getList("feature2=value1"));
			test(0.5, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			orderedSetBuilders.add(getList("feature2=value10"));
			orderedSetBuilders.add(getList("feature1=value2"));
			orderedSetBuilders.add(getList("feature2=value1"));
			test(1.0, orderedSetBuilders);
		}
		{
			List<PropositionListBuilder> orderedSetBuilders = newArrayList();
			orderedSetBuilders.add(getList("feature1=value1"));
			orderedSetBuilders.add(getList("feature1=value2"));
			orderedSetBuilders.add(getList("feature2=value10"));
			orderedSetBuilders.add(getList("feature2=value1"));
			test(Double.NaN, orderedSetBuilders);
		}
	}

	
}
